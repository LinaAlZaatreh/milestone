using System.Drawing;
using System.Drawing.Imaging;

namespace ImageProcessing
{
    public static class ImageProcessor
    {
        public static Bitmap ConvertTo8bpp(Bitmap image)
        {
            int width = image.Width;
            int height = image.Height;
            Bitmap convertedImage = new Bitmap(width, height, PixelFormat.Format8bppIndexed);

            ColorPalette palette = convertedImage.Palette;
            for (int i = 0; i < 256; i++)
            {
                palette.Entries[i] = Color.FromArgb(i, i, i);
            }

            convertedImage.Palette = palette;

            BitmapData convertedImageData = convertedImage.LockBits(
                new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly,
                PixelFormat.Format8bppIndexed);

            BitmapData originalImageData = image.LockBits(
                new Rectangle(0, 0, width, height), ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);

            unsafe
            {
                byte* originalPtr = (byte*)originalImageData.Scan0;
                byte* convertedPtr = (byte*)convertedImageData.Scan0;

                int originalOffset = originalImageData.Stride - width * 3;
                int convertedOffset = convertedImageData.Stride - width;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        byte r = originalPtr[2];
                        byte g = originalPtr[1];
                        byte b = originalPtr[0];

                        byte gray = (byte)(0.3 * r + 0.59 * g + 0.11 * b);
                        convertedPtr[0] = (byte)gray;

                        originalPtr += 3;
                        convertedPtr += 1;
                    }

                    originalOffset++;
                    convertedOffset++;
                }
            }

            image.UnlockBits(originalImageData);
            convertedImage.UnlockBits(convertedImageData);
            image.Dispose();

            return convertedImage;
        }

        public static Bitmap ConvertToBinary(Bitmap grayscaleImage, int threshold)
        {
            int width = grayscaleImage.Width;
            int height = grayscaleImage.Height;

            Bitmap binaryImage = new Bitmap(width, height, PixelFormat.Format8bppIndexed);

            ColorPalette palette = binaryImage.Palette;
            palette.Entries[0] = Color.FromArgb(0, 0, 0);
            palette.Entries[255] = Color.FromArgb(255, 255, 255);
            binaryImage.Palette = palette;

            BitmapData grayscaleData = grayscaleImage.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);

            BitmapData binaryData = binaryImage.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.WriteOnly,
                PixelFormat.Format8bppIndexed);

            unsafe
            {
                byte* grayscaleScan0 = (byte*)grayscaleData.Scan0;
                byte* binaryScan0 = (byte*)binaryData.Scan0;

                int grayStride = grayscaleData.Stride;
                int binaryStride = binaryData.Stride;

                for (int y = 0; y < height; y++)
                {
                    byte* grayscalePointer = grayscaleScan0 + y * grayStride;
                    byte* binaryPointer = binaryScan0 + y * binaryStride;

                    for (int x = 0; x < width; x++)
                    {
                        if (grayscalePointer[x] < threshold)
                            binaryPointer[x] = 0;
                        else
                            binaryPointer[x] = 255;
                    }
                }
            }

            grayscaleImage.UnlockBits(grayscaleData);
            binaryImage.UnlockBits(binaryData);

            return binaryImage;
        }
    }
}
