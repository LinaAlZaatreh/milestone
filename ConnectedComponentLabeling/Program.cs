using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using ImageProcessing;

namespace ConnectedComponentLabeling
{
    public enum ShapeType   
    {
        Square,
        Rectangle,
        Circle,
        Triangle,
        Unknown
    }
    
    public static class ObjectLabeling
    {
        public static void Main(string[] args)
        {
            string directory = Directory.GetCurrentDirectory();
            string imagePath = Path.Combine(directory, "test1.jpg");

            Bitmap originalImage = new Bitmap(imagePath);
            Bitmap grayImage = ImageProcessor.ConvertTo8bpp(originalImage);

            Bitmap image = ImageProcessor.ConvertToBinary(grayImage, 180);

            Bitmap convertedImage = ExtractAndRecognizeShapes(image);

            convertedImage.Save("Converted.bmp", ImageFormat.Bmp);

            originalImage.Dispose();
            grayImage.Dispose();
            image.Dispose();
        }

        public static Bitmap ExtractAndRecognizeShapes(Bitmap image)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            List<Bitmap> extractedShapes = ExtractShapes(image);
            RecognizeShapes(extractedShapes);

            stopwatch.Stop();

            long elapsedMilliseconds = stopwatch.ElapsedMilliseconds;

            Console.WriteLine("Execution time: " + elapsedMilliseconds + " ms");
            return image;
        }

        public static unsafe List<Bitmap> ExtractShapes(Bitmap image)
        {
            int width = image.Width;
            int height = image.Height;

            BitmapData imageData = image.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);

            int stride = imageData.Stride;
            byte* pointer = (byte*)imageData.Scan0;

            int label = 0;
            int[] labels = new int[width * height];

            Stack<(int, int)> stack = new Stack<(int, int)>();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int offset = y * stride + x;

                    if (pointer[offset] == 0 && labels[offset] == 0)
                    {
                        label++;
                        stack.Push((x, y));

                        while (stack.Count > 0)
                        {
                            var (currentX, currentY) = stack.Pop();
                            int currentOffset = currentY * stride + currentX;

                            if (currentX < 0 || currentY < 0 || currentX >= width || currentY >= height)
                                continue;

                            if (pointer[currentOffset] == 0 && labels[currentOffset] == 0)
                            {
                                labels[currentOffset] = label;

                                stack.Push((currentX - 1, currentY));
                                stack.Push((currentX + 1, currentY));
                                stack.Push((currentX, currentY - 1));
                                stack.Push((currentX, currentY + 1));
                                stack.Push((currentX - 1, currentY - 1));
                                stack.Push((currentX + 1, currentY - 1));
                                stack.Push((currentX - 1, currentY + 1));
                                stack.Push((currentX + 1, currentY + 1));
                            }
                        }
                    }
                }
            }

            List<Bitmap> extractedShapes = new List<Bitmap>();

            for (int l = 1; l <= label; l++)
            {
                List<(int, int)> shapePoints = new List<(int, int)>();

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        int offset = y * stride + x;

                        if (labels[offset] == l)
                        {
                            shapePoints.Add((x, y));
                        }
                    }
                }

                if (shapePoints.Count > 0)
                {
                    int minX = shapePoints.Min(p => p.Item1);
                    int minY = shapePoints.Min(p => p.Item2);
                    int shapeWidth = shapePoints.Max(p => p.Item1) - minX + 1;
                    int shapeHeight = shapePoints.Max(p => p.Item2) - minY + 1;

                    Bitmap shape = new Bitmap(shapeWidth, shapeHeight, PixelFormat.Format8bppIndexed);
                    BitmapData shapeData = shape.LockBits(new Rectangle(0, 0, shapeWidth, shapeHeight),
                        ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
                    int shapeStride = shapeData.Stride;
                    byte* shapePointer = (byte*)shapeData.Scan0;

                    for (int y = 0; y < shapeHeight; y++)
                    {
                        byte* shapeRow = shapePointer + y * shapeStride;

                        for (int x = 0; x < shapeWidth; x++)
                        {
                            shapeRow[x] = 255;
                        }
                    }

                    foreach (var point in shapePoints)
                    {
                        int offsetX = point.Item1 - minX;
                        int offsetY = point.Item2 - minY;

                        int destinationOffset = offsetY * shapeStride + offsetX;
                        shapePointer[destinationOffset] = 0;
                    }

                    shape.UnlockBits(shapeData);

                    extractedShapes.Add(shape);
                }
            }

            image.UnlockBits(imageData);
        
            return extractedShapes;
        }
        
       public static void RecognizeShapes(List<Bitmap> shapes)
       {
            foreach (Bitmap shape in shapes)
            {
                ShapeType shapeType = RecognizeShape(shape);
                Console.WriteLine("Shape Type: " + shapeType);
            }
       }

       public static ShapeType RecognizeShape(Bitmap shape)
        {
            int width = shape.Width;
            int height = shape.Height;

            bool isOnBoundary = IsOnBoundary(shape);

            if (isOnBoundary)
            {
                if (width == height)
                {
                    return ShapeType.Square;
                }
                else if (width != height)
                {
                    return ShapeType.Rectangle;
                }
            }
            else if (IsCircle(shape,4))
            {
                return ShapeType.Circle;
            }
            else if (IsTriangle(shape))
            {
                return ShapeType.Triangle;
            }

            return ShapeType.Unknown;
        }

       private static bool IsOnBoundary(Bitmap shape)
       {
           int width = shape.Width;
           int height = shape.Height;

           BitmapData shapeData = shape.LockBits(new Rectangle(0, 0, width, height),
               ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);

           int stride = shapeData.Stride;

           unsafe
           {
               byte* pointer = (byte*)shapeData.Scan0;

               for (int x = 0; x < width; x++)
               {
                   if (pointer[x] != 0)
                   {
                       shape.UnlockBits(shapeData);
                       return false;
                   }
                   if (pointer[(height - 1) * stride + x] != 0)
                   {
                       shape.UnlockBits(shapeData);
                       return false;
                   }
               }

               for (int y = 0; y < height; y++)
               {
                   if (pointer[y * stride] != 0)
                   {
                       shape.UnlockBits(shapeData);
                       return false;
                   }
                   if (pointer[y * stride + width - 1] != 0)
                   {
                       shape.UnlockBits(shapeData);
                       return false;
                   }
               }
           }

           shape.UnlockBits(shapeData);
           return true;
       }
       
       private static bool IsCircle(Bitmap shape, double tolerance)
        {
            int width = shape.Width;
            int height = shape.Height;

            int centerX = (int)Math.Round((width - 1) / 2.0);
            int centerY = (int)Math.Round((height - 1) / 2.0);
            Point center = new Point(centerX, centerY);

            double distanceToBoundaryTop = CalculateDistance(center, new Point(centerX, 0));
            double distanceToBoundaryRight = CalculateDistance(center, new Point(width - 1, centerY));
            double distanceToBoundaryBottom = CalculateDistance(center, new Point(centerX, height - 1));
            double distanceToBoundaryLeft = CalculateDistance(center, new Point(0, centerY));

            bool distancesEqual = Math.Abs(distanceToBoundaryTop - distanceToBoundaryRight) <= tolerance
                                  && Math.Abs(distanceToBoundaryRight - distanceToBoundaryBottom) <= tolerance
                                  && Math.Abs(distanceToBoundaryBottom - distanceToBoundaryLeft) <= tolerance
                                  && Math.Abs(distanceToBoundaryLeft - distanceToBoundaryTop) <= tolerance;

            double aspectRatio = (double)width / height;
            bool isCloseToCircle = Math.Abs(aspectRatio - 1) <= tolerance;

            return distancesEqual && isCloseToCircle;
        }

        private static double CalculateDistance(Point point1, Point point2)
        {
            int dx = point2.X - point1.X;
            int dy = point2.Y - point1.Y;

            return Math.Sqrt(dx * dx + dy * dy);
        }
        private static bool IsTriangle(Bitmap shape)
        {
            int width = shape.Width;
            int height = shape.Height;

            BitmapData shapeData = shape.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadOnly, PixelFormat.Format8bppIndexed);

            int stride = shapeData.Stride;

            unsafe
            {
                byte* pointer = (byte*)shapeData.Scan0;

                int topBoundaryCount = 0;
                int bottomBoundaryCount = 0;
                int leftBoundaryCount = 0;
                int rightBoundaryCount = 0;

                for (int x = 0; x < width; x++)
                {
                    if (pointer[x] == 0)
                    {
                        topBoundaryCount++;
                    }
                }

                for (int x = 0; x < width; x++)
                {
                    if (pointer[(height - 1) * stride + x] == 0)
                    {
                        bottomBoundaryCount++;
                    }
                }

                for (int y = 0; y < height; y++)
                {
                    if (pointer[y * stride + width - 1] == 0)
                    {
                        rightBoundaryCount++;
                    }
                }

                for (int y = 0; y < height; y++)
                {
                    if (pointer[y * stride] == 0)
                    {
                        leftBoundaryCount++;
                    }
                }

                shape.UnlockBits(shapeData);

                int blackBoundaryCount = 0;

                if (topBoundaryCount >= width - 2)
                    blackBoundaryCount++;

                if (bottomBoundaryCount >= width - 2)
                    blackBoundaryCount++;

                if (leftBoundaryCount >= height - 2)
                    blackBoundaryCount++;

                if (rightBoundaryCount >= height - 2)
                    blackBoundaryCount++;

                return blackBoundaryCount <= 2;
            }
        }

    }
}
